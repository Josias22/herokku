from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('searchbook', views.search, name='search'),
    path('data', views.func_data, name='data'),
    # dilanjutkan ...
]