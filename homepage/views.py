from django.shortcuts import render
from django.http import JsonResponse
import requests
import json
# Create your views here.
def index(request):
    return render(request, 'index.html')


def search(request):
    return render(request, 'searchbox.html')

def func_data(request):
    args = request.GET['q']
    get_url = 'https://www.googleapis.com/books/v1/volumes?q='+args
    r = requests.get(get_url)

    data = json.loads(r.content)
    return JsonResponse(data, safe=False)
